require 'rubygems'
require 'bundler'
Bundler.require

Geocoder.configure(lookup: :cloudmade, api_key: ENV['CLOUDMADE_API_KEY'])
results = Geocoder.search ARGV[0]
if results.empty?
  puts "No results found"
else
  result = results[0]
  puts "Lat: #{result.latitude}, Lng: #{result.longitude}"
end
